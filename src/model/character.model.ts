export class Character {
    public mouvements: any[];
    public highlighted = false;
    public color: any;
    public x: number;
    public y: number;
}