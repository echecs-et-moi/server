import { Socket } from 'socket.io';

export default interface GameSocket extends Socket{
    gameId: String;
    player1: String;
    player2: String;
}