import { Move } from "./move.model";


export default class Game  {
    started: Boolean;
    joinCode: string;
    moves: Move[];
    player1: String;
    player2: String;

    constructor() {
        this.started = false;
        this.joinCode = randomString(4);
        this.moves = [];
    }

}

function randomString(length) {
    return Math.round((Math.pow(36, length + 1) - Math.random() * Math.pow(36, length))).toString(36).slice(1);
}