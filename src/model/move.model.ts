import { Character } from './character.model';

export class Move{
    public oldMove: Character;
    public newMove: Character;
    public pieceTaken: any;
}