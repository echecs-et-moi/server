import { EventEmitter } from "events";
import GameSocket from "../model/game-socket.model";
import { Move } from "../model/move.model";

export default class GameLogic extends EventEmitter {
  private currentPlayer: GameSocket;

  private moves: Move[] = [];

  private moving: boolean = false;

  constructor(private players: GameSocket[]) {
    super();

    // TODO: randomize the first player ?
    // const firstPlayer = Math.round(Math.random());
    this.currentPlayer = players[0]; // Always the game creator
  }

  public getCurrentPlayer(): GameSocket {
    return this.currentPlayer;
  }

  public getOpponent(): GameSocket {
    return this.players
      .filter(player => player.id !== this.currentPlayer.id)
      .shift();
  }

  public isMoving() {
    return this.moving;
  }

  public pushMove(move: Move): boolean {
    if (this.moving) return false;

    this.moving = true;

    /**
     * Only handle the moves with the backend then broadcast it to both players
     * The front do not have to move before the back confirms the move
     *
     * It will allow to check if the move is correct or not
     */
    this.moves.push(move);

    // Check the game state, if any winner emit the event "gameEnded"
    // with the GameSocket that wins

    this.updateOpponent();
    this.moving = false;

    return true;
  }

  private updateOpponent() {
    this.currentPlayer = this.getOpponent();
  }
}
