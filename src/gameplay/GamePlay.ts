import { EventEmitter } from "events";
import { isEmpty } from "lodash";
import GameSocket from "../model/game-socket.model";
import { Move } from "../model/move.model";
import GameLogic from "./GameLogic";
import randomstring from "randomstring";

export enum GAMEPLAY_STATUS {
  PENDING,
  PLAYING
}

/**
 * Length of gameID
 */
const LENGTH: number = 4;

/**
 *
 */
export default class GamePlay extends EventEmitter {
  /**
   * Unique game identifier
   */
  private gameId: string;

  /**
   * Game players
   */
  private players: GameSocket[] = [];

  private gameLogic: GameLogic;

  /**
   * Current gameplay state
   */
  private status: GAMEPLAY_STATUS = GAMEPLAY_STATUS.PENDING;

  /**
   *
   * @param socket
   */
  constructor(private io: SocketIO.Server) {
    super();

    this.initialize();
  }

  /**
   *
   * @param socket
   */
  public addPlayer(socket: GameSocket) {
    if (this.players.length === 2) {
      throw new Error(`Two players have already join this game`);
    }

    // First player, send him the game ID
    // Before joining the game room
    if (this.players.length === 0) {
      socket.emit("codeGame", this.getGameId());
    }

    this.players.push(socket);
    this.registerSocketEvents(socket);

    /**
     * If 2 players have joined, start the game
     */
    if (this.players.length === 2) {
      this.start();
    }
  }

  /**
   * Get the current game identifier
   */
  public getGameId(): string {
    return this.gameId;
  }

  /**
   * Initialize the gameplay:
   *    - Creates a unique game identifier
   */
  public initialize(force: boolean = false): void {
    if (!isEmpty(this.gameId) && !force) return;
    this.gameId = randomstring.generate({
      length: 4,
      capitalization: "uppercase"
    });
  }

  private start() {
    this.gameLogic = new GameLogic(this.players);

    this.gameLogic.on("gameEnded", this.handleGameEnded.bind(this));

    this.io.to(this.gameId.toString()).emit("gameStart", this.gameId);

    this.status = GAMEPLAY_STATUS.PLAYING;
  }

  private registerSocketEvents(socket: GameSocket): void {
    // Restrict user to the game channel
    socket.join(this.gameId.toString());

    socket.on("move", this.handleMove.bind(this));

    socket.on("disconnect", () => this.handleDisconnect(socket));
  }

  private handleMove(move: Move): void {
    if (this.gameLogic.isMoving()) return;

    // Get the opponent/current before pushing the next move
    const current = this.gameLogic.getCurrentPlayer();
    const opponent = this.gameLogic.getOpponent();

    const hasMoved = this.gameLogic.pushMove(move);

    if (hasMoved) opponent.emit("move", move);
    else current.emit("badMove");
  }

  private handleGameEnded(socket?: GameSocket): void {
    if (!isEmpty(socket)) {
      // We have a winner
      this.io
        .to(this.gameId.toString())
        .emit("gameEnded", { winner: socket.id });
    }

    this.players.forEach(player => player.leave(this.gameId.toString()));

    // Free memory
    delete this.gameLogic;

    // To inform the server to destory this gameplay
    this.emit("gameEnded");
  }

  private handleDisconnect(socket: GameSocket): void {
    // First remove the player from the game logic

    // Then remove it from the game play
    this.players = this.players.filter(player => player.id === socket.id);

    // No more players in this game, destroy it
    if (this.players.length === 0) return this.handleGameEnded(null);

    // Else, let the the left player waits for another player
    this.io
      .to(this.gameId.toString())
      .emit("gameSuspended", { playerLeaves: true });

    this.status = GAMEPLAY_STATUS.PENDING;
  }
}
