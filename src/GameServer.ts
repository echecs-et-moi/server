import _ from "lodash";
import express from "express";
import socketIO from "socket.io";
const fs = require('fs');
const http = require('http');
const https = require('https');

import GamePlay from "./gameplay/GamePlay";
import GameSocket from "./model/game-socket.model";

/**
 *
 */
export class GameServer {
  public static readonly DEFAULT_PORT: number = 8080;

  private _port: number;

  /**
   * HTTP/WebSocket server
   */
  private server: any; // HTTP or HTTPS
  private app: express.Application;
  private io: SocketIO.Server;

  /**
   * Games running
   */
  private games: { [key: string]: GamePlay } = {};

  constructor() {
    this._port = Number(process.env.APP_PORT);
    this.app = express();
    if (process.env.NODE_ENV === 'production') {
      console.log("Running on production");
      // Certificate
      const privateKey = fs.readFileSync('/etc/letsencrypt/live/echecsetmoi.fr/privkey.pem', 'utf8');
      const certificate = fs.readFileSync('/etc/letsencrypt/live/echecsetmoi.fr/cert.pem', 'utf8');
      const ca = fs.readFileSync('/etc/letsencrypt/live/echecsetmoi.fr/chain.pem', 'utf8');

      const credentials = {
        key: privateKey,
        cert: certificate,
        ca: ca
      };
      this.server = https.createServer(credentials, this.app);
    } else {
      console.log("Running on development");
      this.server = http.createServer(this.app);
    }
  }

  public get port() {
    return typeof this._port === "number" && !isNaN(this._port)
      ? this._port
      : GameServer.DEFAULT_PORT;
  }

  public start() {
    this.server.listen(this.port, () => {
      console.log(`1f525 Server running on port ${this.port}`);
    });
    
    this.io = socketIO(this.server);

    this.app.get('/', (req, res) => res.send('Hello World!'))

    this.io.on("connect", (socket: GameSocket) => {
      socket.on("createCodeGame", () => {
        const game = this.createGamePlay();
        game.addPlayer(socket); // First player added
      });

      socket.on("joinGame", code => {
        if (!_.has(this.games, code)) {
          return socket.emit("badJoinCode");
        }

        const game = _.get(this.games, code);
        game.addPlayer(socket); // Auto start handle by the gameplay instance
      });

      socket.on("gameLeave", () => { }); // TODO

      // Send to the opponent the gameId to allow
      // another user to join the game
      socket.on("disconnect", () => { }); // TODO
    });
  }

  private createGamePlay(): GamePlay {
    const game = new GamePlay(this.io);
    let gameId = game.getGameId();

    while (_.has(this.games, gameId)) {
      game.initialize(true); // force
      gameId = game.getGameId();
    }

    this.games[gameId] = game;

    return game;
  }
}
