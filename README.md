# NodeJS server for Echecs & moi application 

### To work on this project:
First pull the application with git bash using this command line :
```
git pull git@gitlab.com:echecs-et-moi/server.git
```
or
```
git pull https://gitlab.com/echecs-et-moi/server.git
```
Then install all project dependencies : 
```
cd server/
npm install
```
Then use this command line to build project and run the server : 
```
npm run rerun
```
